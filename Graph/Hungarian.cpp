LL n,m;
const LL MAXN = 500;
LL dist[MAXN + 5][MAXN + 5];
LL u[MAXN + 5],v[MAXN + 5];
bool used[MAXN + 5];
LL p[MAXN + 5],way[MAXN + 5];
LL minv[MAXN + 5];
LL ans[MAXN + 5];
 
// kalo sizenya n < m, pad sampe sama.
// ini hungarian min cost, kalo mo max cost kasih -1
LL Assign(){
    // m = n
    for(int i = 1;i <= n;i++){
        p[0] = i;
        LL pos = 0;
        for(int j = 0;j <= m;j++)
            minv[j] = LINF,used[j] = 0;
        do{
            used[pos] = 1;
            LL curp = p[pos],delta = LINF, nx;
            for(int j = 1;j <= m;j++){
                if(!used[j]){
                    LL cur = dist[curp][j]-u[curp]-v[j];
                    if(cur < minv[j])
                        minv[j] = cur,way[j] = pos;
                    if(minv[j] < delta)
                        delta = minv[j], nx = j;     
                }
            }
            for(int j = 0 ; j <= m; j++){
                if(used[j])
                   u[p[j]] += delta,v[j] -= delta; 
                else
                   minv[j] -= delta;
            }
            pos = nx;           
        }while(p[pos] != 0); 
        do{
            LL nx = way[pos];
            p[pos] = p[nx];
            pos = nx;
        }while(pos);
    }
    for(int i = 1;i <= n;i++)
        ans[p[i]] = i;
    return -v[0];
}