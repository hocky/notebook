void hielholzer(LL pos){
	while(edge[pos].size()){
		LL nx = *edge[pos].begin();
		edge[pos].erase(edge[pos].begin());
		edge[nx].erase(edge[nx].find(pos));
		dfs(nx);
	}
	path.pb(pos);
}