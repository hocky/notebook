// Chinese remainder theorem (special case): find z such that
// z % x = a, z % y = b.  Here, z is unique modulo M = lcm(x,y).
// Return (z,M).  On failure, M = -1.
pair<int, int> chinese_remainder_theorem(int x, int a, int y, int b) {
  int s, t;
  int d = extended_euclid(x, y, s, t);
  // printf("%lld %lld %lld %lld %lld\n",x,a,y,b,d);
  if (a % d != b % d) return make_pair(0, -1);
  int md = x * y;
  int res = (s * b) % md;
  res = (res * x) % md;
  int res2 = (t * a) % md;
  res2 = (res2 * y) % md;
  return make_pair(mod(res + res2, md) / d, x * y / d);
}

// Chinese remainder theorem: find z such that
// z % x[i] = a[i] for all i.  Note that the solution is
// unique modulo M = lcm_i (x[i]).  Return (z,M).  On 
// failure, M = -1.  Note that we do not require the a[i]'s
// to be relatively prime.
pair<int, int> chinese_remainder_theorem(const vector<int> &x, const vector<int> &a) {
  pair<int, int> ret = make_pair(x[0], a[0]);
  for (int i = 1; i < x.size(); i++) {
    ret = chinese_remainder_theorem(ret.first, ret.second, x[i], a[i]);
    swap(ret.first, ret.second);
    if (ret.first == -1) break;
  }

  return ret;
}
