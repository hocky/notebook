struct state{
  int link,len;
  int nex[LETTER];
  
  void reset(){
    link = len = -1;
    memset(nex,-1,sizeof nex);
  }
};

struct automata{
  int last,sz;
  state node[2 * MAXN];
  char str[MAXN];
 
  void init(){
    sz = last = 0;
    node[0].reset();
    node[0].len = 0;
    node[0].link = -1;
    sz++;
  }
  
  void extend(char c){
    c -= 'A';
    int cur = sz++;
    node[cur].reset();
    node[cur].len = node[last].len + 1;
  
    int p;
    for(p = last ; p != -1 && node[p].nex[c] == -1 ; p = node[p].link)
      node[p].nex[c] = cur;
      
    if(p == -1)
      node[cur].link = 0;
    else{
      int q = node[p].nex[c];
      if(node[p].len + 1 == node[q].len)
        node[cur].link = q;
      else{
        int clone = sz++;
        node[clone].reset();
        node[clone].len = node[p].len + 1;
        for(int i = 0 ; i < LETTER ; i++)
          node[clone].nex[i] = node[q].nex[i];
        node[clone].link = node[q].link;
        for(; p != -1 && node[p].nex[c] == q ; p = node[p].link)
          node[p].nex[c] = clone;
        node[q].link = node[cur].link = clone;
      }
    }
    
    last = cur;
  }
}  
