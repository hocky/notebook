struct dte{
    LL to,cost;
    bool operator<(const dte &other)const{
        if(cost != other.cost) return cost < other.cost;
        return to < other.to;
    }
};
LL par[2505];
LL findpar(LL now){
    if(par[now] == now) return now;
    return par[now] = findpar(par[now]);
}
LL n;
LL best_edge[2505];

struct heap_node{
    dte val; LL lazy;
    LL child[2];
};
LL cnt_heap;
heap_node randomized_heap[10000005];

inline void prop(LL idx){
    if(randomized_heap[idx].lazy == 0) return;
    randomized_heap[idx].val.cost += randomized_heap[idx].lazy;
    if(randomized_heap[idx].child[0] != 0) randomized_heap[randomized_heap[idx].child[0]].lazy += randomized_heap[idx].lazy;
    if(randomized_heap[idx].child[1] != 0) randomized_heap[randomized_heap[idx].child[1]].lazy += randomized_heap[idx].lazy;
    randomized_heap[idx].lazy = 0;
}

LL merge(LL a, LL b){
    if(!a) return b;
    if(!b) return a;
    prop(a); prop(b);
    if(randomized_heap[b].val < randomized_heap[a].val) swap(a,b);
    if(rand()&1) swap(randomized_heap[a].child[0],randomized_heap[a].child[1]);
    randomized_heap[a].child[0] = merge(randomized_heap[a].child[0],b);
    return a;
}

LL insert(LL idx, dte val){
    randomized_heap[++cnt_heap].val = val;
    return merge(idx,cnt_heap);
}

LL erase(LL idx){
    prop(idx);
    return merge(randomized_heap[idx].child[0],randomized_heap[idx].child[1]);
}

LL edge[2505];

int main(){
    srand(time(NULL));
    memset(best_edge,-1,sizeof(best_edge));
    fastll(n);
    for(int i = 1;i <= n;i++) par[i] = i;
    for(int i = 1;i <= n;i++){
        LL x,s; fastll(x); fastll(s);
        if(i != x) edge[i] = insert(edge[i],{x,s});
        for(int j = 0;j <= n;j++){
            fastll(s);
            if(i == j) continue;
            edge[i] = insert(edge[i],{j,s});
        }
    }
    LL ans = 0;
    //Chi Liu Edmonds + Randomized Heap
    for(int i = 1;i <= n;i++){
        if(findpar(i) == 0) continue;
        stack <LL> path; path.push(i);
        LL cntloop = 0;
        while(1){
            LL u = path.top();
            dte nx = randomized_heap[edge[u]].val;
            edge[u] = erase(edge[u]);
            LL v = findpar(nx.to);
            if(u == v) continue;
            ans += nx.cost;
            best_edge[u] = nx.cost;
            if(v == 0) break;
            if(best_edge[v] == -1) path.push(v);
            else{
                vector <LL> tmp;
                while(1){
                    LL now = path.top(); path.pop();
                    tmp.pb(now);
                    if(findpar(now) == v) break;
                    par[par[now]] = v;
                }
                path.push(v);
                LL titip = 0;
                for(int j = 0;j < tmp.size();j++){
                    LL now = tmp[j];
                    randomized_heap[edge[now]].lazy -= best_edge[now];
                    titip = merge(titip,edge[now]);
                    edge[now] = 0;
                }
                edge[v] = titip;
            }
        }
        while(!path.empty()){
            par[findpar(path.top())] = 0;
            path.pop();
        }
    }
    printf("%lld\n",ans);
    return 0;
}