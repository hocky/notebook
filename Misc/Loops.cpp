  submask :
    for(LL tmp = now;tmp;tmp = ((tmp-1)&now)){
        tmp is all submask of now
    }

  supermask :
    for(LL tmp = now;tmp < (1LL<<(max_digit));tmp = ((tmp+1)|now)){
        tmp is all supermask of now
    }

  MSB loop :
    for(;pos; pos -= (pos&(-pos))){
        //pos is all BIT pos
    }