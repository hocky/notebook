
struct dt{
    LL a,b;
    bool operator<(const dt &other)const{
        if(a == other.a) return b > other.b;
        return a < other.a;
    }
};
struct line{
    LL m,c,idx;
};

double getintersection(line a,line b){
    return (double)(a.c-b.c)/(double)(a.m-b.m);
}

LL gety(line a, LL x){
    return a.m*x+a.c;
}

LL N;
LL curk;
LL par[100005];
dt isi[100005];
deque <line> hull;
dt isiold[100005];
LL memo[100005],cnt;

void cari(LL curslope){
    hull.clear();
    //dp ch, g(x) = f(x)-c(x)
    // where f(x) is a function which has non decreasing slope
    cnt = 0;
    for(int i = 1;i <= N;i++){
        LL tmp;
        if(isi[i-1].b < isi[i].a) tmp = 0;
        else tmp = (isi[i-1].b-isi[i].a+1)*(isi[i-1].b-isi[i].a+1);
        line nx = {2*isi[i].a,memo[i-1]-(2*isi[i].a)+(isi[i].a*isi[i].a)+1-tmp,i-1};
        while(hull.size() > 1){
            LL blkg = hull.size()-1;
            if(getintersection(hull[blkg],hull[blkg-1]) < getintersection(hull[blkg],nx)) break;
            hull.popb();
        }
        hull.pb(nx);
        while(hull.size() > 1){
            if(gety(hull[0],-isi[i].b) < gety(hull[1],-isi[i].b)) break;
            hull.popf();
        }
        memo[i] = gety(hull[0],-isi[i].b)+(isi[i].b*isi[i].b)+(2*isi[i].b)+curslope;
        par[i] = hull[0].idx;
    }
    LL now = N;
    while(now){
        // cout << "here is " << now << endl;
        cnt++;
        now = par[now];
    }
}

long long take_photos(int n, int m, int k, vector<int> r, vector<int> c) {
    for(int i = 0;i < n;i++){
        if(r[i] > c[i]) swap(r[i],c[i]);
        isiold[i+1] = {r[i],c[i]};
    }
    sort(isiold+1,isiold+1+n);
    LL cur = -1;
    LL newn = 0;
    isi[0] = {-1,-1};
    for(int i = 1;i <= n;i++){
        if(cur == -1 || isiold[cur].b < isiold[i].b){
            isi[++newn] = isiold[i];
            cur = i;
        }else continue;
    }

    N = newn;
    LL kiri = 0; LL kanan = 1000000000000000000LL;
    //Makin dibesarin midnya, k nya akan makin kecil
    //Makin kecil midnya, k nya semakin besar
    //Cari cnt pertama yang sama dengan k
    while(kiri < kanan){
        LL mid = (kiri+kanan+1)>>1;
        cari(mid);
        if(cnt >= k) kiri = mid;
        else kanan = mid-1;
    }
    cari(kiri);
    return memo[N]-k*kiri;
}