
__gcd(a,b)
__builtin_ffs(a) first on bit
__builtin_clz(a) count leading zero
__builtin_ctz(a) count trailing zero
__builtin_popcount(a) numbers of on bits
v.erase(unique(v.begin(),v.end()),v.end());

#pragma comment(linker, "/stack:200000000")
#pragma GCC target("avx,avx2,fma")
#pragma GCC optimize("O3")
#pragma GCC optimize("Ofast")
#pragma GCC optimize("unroll-loops")

const int INF = 1061109567;
const LL LINF = 4557430888798830399LL;

inline void open(string a){
    freopen((a+".in").c_str(),"r",stdin);
    freopen((a+".out").c_str(),"w",stdout);
}

int main(){
    //Input Parsing
    vector <string> all;
    stringstream ss;
    string s;

    getline(cin,s);
    ss.str(s);
    while(now >> s) all.pb(s);
    for(int i = 0;i < all.size();i++) cout << all[i] << endl;

    return 0;
}