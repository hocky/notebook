const int mxsz = 2e5 + 10;
const int MX = 2e5 + 1;
LL fac[mxsz], inv[mxsz], finv[mxsz];
int n, m;

void prec(){ // factorial / inverse
    fac[0] = inv[0] = finv[0] = 1;
    fac[1] = inv[1] = finv[1] = 1;
    for (int i = 2; i <= MX; i++){
        fac[i] = (fac[i-1] * i) % mod;
        inv[i] = mod - (mod/i) * inv[mod%i] % mod;
        finv[i] = (finv[i-1] * inv[i]) % mod;
    }
}
