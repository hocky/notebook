mt19937 rng(time(NULL));

struct node{
	LL prior,sz,l,r,val;
	vector <LL> agr;
	bool lazy;
}treap[100005];

struct dt{
	LL kiri,kanan;
};

char isi[100005];
LL tridx,root,n,q;

LL expo(LL a, LL b){
	if(b == 0) return 1;
	LL tmp = expo(a,b/2);
	tmp = (tmp*tmp)%MOD;
	if(b&1) tmp = (tmp*a)%MOD;
	return tmp;
}

void prop(LL now){
	if(now == 0 || treap[now].lazy == 0) return;
	treap[now].lazy = 0;
	swap(treap[now].l,treap[now].r);
	if(treap[now].l) treap[treap[now].l].lazy ^= 1;
	if(treap[now].r) treap[treap[now].r].lazy ^= 1;
}

void updsz(LL now){
	if(now == 0) return;
	treap[now].sz = treap[treap[now].l].sz+treap[treap[now].r].sz+1;
	for(int i = 0;i < 26;i++){
		treap[now].agr[i] = treap[treap[now].l].agr[i]+treap[treap[now].r].agr[i];
	}
	treap[now].agr[treap[now].val]++;
}

dt split(LL now,LL add, LL tar){
	if(now == 0) return {0,0};
	prop(now);
	dt ret;
	LL curkey = add+treap[treap[now].l].sz;
	if(curkey <= tar){
		dt tmp = split(treap[now].r,curkey+1,tar);
		treap[now].r = tmp.kiri;
		ret = {now,tmp.kanan};
	}else{
		dt tmp = split(treap[now].l,add,tar);
		treap[now].l = tmp.kanan;
		ret = {tmp.kiri,now};
	}
	updsz(now);
	return ret;
}

LL merge(LL l, LL r){
	LL ret;
	prop(l); prop(r);
	if(l == 0) ret = r;
	else if(r == 0) ret = l;
	else{
		if(treap[l].prior > treap[r].prior){
			treap[l].r = merge(treap[l].r,r);
			ret = l;
		}else{
			treap[r].l = merge(l,treap[r].l);
			ret = r;
		}
	}
	updsz(ret);
	return ret;
}

void insert(LL tar){
	LL cur = ++tridx;
	treap[cur].prior = rng();
	treap[cur].agr.resize(26);
	treap[cur].val = (isi[tar]-'a');
	root = merge(root,cur);
}

void balik(LL l, LL r){
	dt tmp = split(root,1,l-1);
	dt tmpa = split(tmp.kanan,1,r-l+1);
	treap[tmpa.kiri].lazy ^= 1;
	root = merge(tmp.kiri,tmpa.kiri);
	root = merge(root,tmpa.kanan);
}