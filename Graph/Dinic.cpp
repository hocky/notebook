
struct dt{
    LL to,cap,backidx;
};
struct dtb{
    LL pos,step;
};
LL maxflow;
LL n,m,s,e;
LL st[5005];
LL level[5005];
LL profit[10005];
queue <dtb> antri;
vector <dt> edge[5005];

void make_edge(LL u, LL v, LL capto, LL backcap){
    LL szu = edge[u].size();
    LL szv = edge[v].size();
    edge[u].pb({v,capto,szv});
    edge[v].pb({u,backcap,szu});
    return;
}

void bfs(dtb now){
    for(int i = 0;i < edge[now.pos].size();i++){
        dt nx = edge[now.pos][i];
        if(level[nx.to]) continue;
        if(nx.cap == 0) continue;
        level[nx.to] = now.step+1;
        antri.push({nx.to,now.step+1});
    }
    return;
}

LL dfs(LL now, LL bcap){
    if(now == e) return bcap;
    for(;st[now] < edge[now].size();st[now]++){
        dt nx = edge[now][st[now]];
        if(level[nx.to] != level[now]+1) continue;
        if(nx.cap == 0) continue;
        LL ret = dfs(nx.to,min(bcap,nx.cap));
        if(ret == 0) continue;
        edge[now][st[now]].cap -= ret;
        edge[nx.to][nx.backidx].cap += ret;
        return ret;
    }
    return 0;
}

int main(){
    fastll(n); fastll(m);
    s = 0; e = n+1;
    LL ans = 0;
    for(int i = 1;i <= n;i++){
        fastll(profit[i]);
        if(profit[i] < 0){
            make_edge(i,e,-profit[i],0);
        }else{
            ans += profit[i];
            make_edge(s,i,profit[i],0);
        }
    }
    for(int i = 1;i <= m;i++){
        LL u,v; cin >> u >> v;
        make_edge(u,v,LINF,0);
    }
    while(1){
        for(int i = s;i <= e;i++) level[i] = st[i] = 0;
        antri.push({s,1});
        level[s] = 1;
        while(!antri.empty()){
            dtb tmp = antri.front();
            antri.pop();
            bfs(tmp);
        }
        if(level[e] == 0) break;
        LL curflow;
        while(curflow = dfs(s,LINF)) maxflow += curflow;
    }
    printf("%lld\n",ans-maxflow);
    return 0;
}