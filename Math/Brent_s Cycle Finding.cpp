int f(int x)  { // state transition from x 
  return // something; 
}

pair<int, int> brent(int x0) { 
  int lambda = 0, mu = 0, power; 
	int tortoise = x0; 
	int hare = f(x0);
	// finding succesive power of two 
	// find lambda 
	lambda = power = 1; 
	while (tortoise != hare) { 
		if (power == lambda) { 
			power <<= 1; 
			tortoise = hare; 
			lambda = 0; 
		} 
		hare = f(hare); 
		++lambda; 
	}
  // find mu 
	tortoise = hare = x0; 
	for (int i = 0; i < lambda; ++i) 
		hare = f(hare); 
	while (tortoise != hare) { 
		tortoise = f(tortoise); 
		hare = f(hare); 
		++mu; 
	} 
	return make_pair(lambda, mu); 
}
 
