import java.io.*;
import java.util.*;
import java.math.*;

public class Main {
    static FastInput in;
    static BufferedWriter out;

    public static void main(String[] args) throws IOException {
        // Fast IO with BufferedReader and BufferedWriter
        in = new FastInput();
        out = new BufferedWriter(new OutputStreamWriter(System.out));

        int x = in.nextInt();
        int y = in.nextInt();

        printf("sum = %d\n", x + y);

        // Big Integer
        BigInteger A = new BigInteger(in.next()); // String init
        BigInteger B = BigInteger.valueOf(in.nextLong()); // Integer init
        BigInteger sum = A.add(B); // Addition
        BigInteger mul = A.multiply(B); // Multiplication

        if (sum.compareTo(mul) < 0) { // Comparation
            printf("%d %s\n", sum.longValue(), mul.toString()); // get Integer/String representation
        }

        out.close();
    }

    static void printf(String format, Object ...args) throws IOException{
        out.write(String.format(format, args));
    }

    static class FastInput {
        BufferedReader br;
        StringTokenizer st;

        FastInput() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
            return Integer.parseInt(next());
        }
        long nextLong() {
            return Long.parseLong(next());
        }
        double nextDouble() {
            return Double.parseDouble(next());
        }
        String nextLine() {
            String s = "";
            try {
                s = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return s;
        }
    }
}
