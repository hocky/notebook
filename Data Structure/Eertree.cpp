struct node{
  int nxt[LETTER]; // penunjuk ke node yang bisa dituju jika diappend huruf ke i di depan dan belakang
  int occ; //occurence
  int sufflink; //link ke proper suffix terbesar
  int len; //length
  
  node() {
    memset(nxt,0,sizeof nxt);
  }
};
 
node tree[MAXN];
int now,last;
char s[MAXN];
 
inline void extend(int pos){
  int cur=now, curlen;
  int letter = s[pos] - 'a';
  while(1){
    curlen = tree[cur].len;
    if(pos - 1 - curlen >= 0 && s[pos - 1 - curlen] == s[pos]) // kalau ini bisa diappend s[pos] di depan dan belakang
      break;
    cur = tree[cur].sufflink; // pindah ke proper suffix terbesarnya; karena yang diproses substring 
  }
  // sekarang ini suffix palindrome terbesar yang berakhir di pos
  if(tree[cur].nxt[letter]){ //kalau sudah ada nodenya
    now = tree[cur].nxt[letter];
    tree[now].occ++;
    return;
  }
  
  last++;
  now = last;
  tree[now].len = tree[cur].len+2;
  tree[cur].nxt[letter] = now;
  tree[now].occ = 1; // kalau mau tau kemunculan tiap substring, di akhir di-"drop" ; occ berfungsi sebagai lazy
  if(tree[now].len == 1){
    tree[now].sufflink = 2; // ke root yang valid
    return;
  }
  while(1){ 
    cur = tree[cur].sufflink;
    curlen = tree[cur].len;
    if(pos - 1 - curlen >= 0 && s[pos - 1 - curlen] == s[pos]){ // ini merupakan proper suffix terbesar
      tree[now].sufflink = tree[cur].nxt[letter];
      break;
    } 
  }
}
 
inline void init(){
  tree[1].len=-1; tree[1].sufflink=1;
  tree[2].len=0; tree[2].sufflink=1;
  now=last=2;
}
