struct dte{
    LL to, cost, cap, flow, backEdge;
};
struct MCMF {
  LL s, e, n;
  vector<vector<dte>> edge;
  MCMF(LL _s, LL _e, LL _n) {
    s = _s, e = _e, n = _n;
    edge.resize(n);
  }
  void addEdge(LL u, LL v, LL cap, LL cost) {
    dte e1 = { v, cost, cap, 0,(LL)(edge[v].size())};
    dte e2 = { u, -cost, 0, 0,(LL)(edge[u].size())};
    edge[u].push_back(e1); edge[v].push_back(e2);
  }
  PLL minCostMaxFlow() {
    LL flow = 0, cost = 0;
    vector<LL> vis(n), from(n), from_edge(n), dist(n);
    deque<LL> q;
    while (1) {
      for (int i = 0; i < n; i++)
        vis[i] = 2, dist[i] = LINF, from[i] = -1;
      vis[s] = 1; q.clear(); q.push_back(s); dist[s] = 0;
      while (!q.empty()) {
        LL pos = q.front(); q.pop_front(); vis[pos] = 0;
        for (int i = 0; i < edge[pos].size(); i++) {
          dte nx = edge[pos][i];
          if (nx.flow >= nx.cap || dist[nx.to] <= dist[pos] + nx.cost)
            continue;
          LL to = nx.to; dist[to] = dist[pos] + nx.cost;
          from[to] = pos; from_edge[to] = i;
          if (vis[to] == 1) continue;
          if (!vis[to] || (!q.empty() && dist[q.front()] > dist[to]))
            q.push_front(to);
          else q.push_back(to);
          vis[to] = 1;
        }
      }
      if (dist[e] == LINF) break;
      LL it = e, addflow = LINF;
      while (it != s) {
        addflow = min(addflow, edge[from[it]][from_edge[it]].cap- edge[from[it]][from_edge[it]].flow);
        it = from[it];
      }
      it = e;
      while (it != s) {
        edge[from[it]][from_edge[it]].flow += addflow;
        edge[it][edge[from[it]][from_edge[it]].backEdge].flow -= addflow;
        cost += edge[from[it]][from_edge[it]].cost * addflow;
        it = from[it];
      }
      flow += addflow;
    }
    return {cost,flow};
  }
};