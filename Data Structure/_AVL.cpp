class AVL{
  class node{
    public:
      int val,num,h;
      node *L,*R;
      node(int _val){
        L=R=NULL;
        val=_val;
        num=1;
        h=1;
      }
  };
  public:
    node *top;
    AVL(){
      top=new node(INF);
    }
    int getH(node *cur){
      if(cur==NULL)return 0; return cur->h;
    }
    int getbal(node *cur){
      return getH(cur->L)-getH(cur->R);
    }
    int getNum(node *cur){
      if(cur==NULL)return 0;
      else return cur->num;
    }
    void setNum(node *cur){
      cur->num=1+getNum(cur->L)+getNum(cur->R);
    }
    node *RotL(node *x){
      node *y=x->R;
      
      x->R=y->L;
      y->L=x;
      
      x->h=max(getH(x->L),getH(x->R))+1;
      y->h=max(getH(y->L),getH(y->R))+1;
      setNum(x); setNum(y);
      
      return y;
    }
    node *RotR(node *x){
      node *y=x->L;
 
      x->L=y->R;
      y->R=x;  
      x->h=max(getH(x->L),getH(x->R))+1;
      y->h=max(getH(y->L),getH(y->R))+1;
      setNum(x); setNum(y);
      
      return y;
    }
    node *ins(node *cur,int _val){
      if(cur==NULL)return (new node(_val));
      if(cur->val==_val){
        return cur;
      }
      
      if(_val<cur->val)cur->L=ins(cur->L,_val);
      else cur->R=ins(cur->R,_val);
      
      cur->h=max(getH(cur->L),getH(cur->R))+1;
      setNum(cur);
      
      int bal=getbal(cur);
      
      if(bal==2){
        if(getbal(cur->L)==-1)
          cur->L=RotL(cur->L);
            return RotR(cur);
      }else if(bal==-2){
        if(getbal(cur->R)==1)
          cur->R=RotR(cur->R);
            return RotL(cur);
      }
      
      return cur;
    }
    void insert(int val){
      top=ins(top,val);
    }
    void getL(node *cur,node *T){
      if(cur->L==NULL){
        swap(cur->val,T->val);
      }else{
        getL(cur->L,T);
      }
    }
    node *del(node *cur,int _val){
      if(cur==NULL)return NULL;
      if(cur->val==_val){
        if(cur->L==NULL || cur->R==NULL){
          node *ret;
          if(cur->L==NULL)ret=cur->R;
          else ret=cur->L;
          free(cur);
          return ret;
        }else{
          getL(cur->R,cur);
          cur->R=del(cur->R,_val);
        }
      }else{
        if(_val<cur->val)cur->L=del(cur->L,_val);
        else cur->R=del(cur->R,_val);
      }
      
      cur->h=max(getH(cur->L),getH(cur->R))+1;
      setNum(cur);
      
      int bal=getbal(cur);
      
      if(bal==2){
        if(getbal(cur->L)==-1)
          cur->L=RotL(cur->L);
            return RotR(cur);
      }else if(bal==-2){
        if(getbal(cur->R)==1)
          cur->R=RotR(cur->R);
            return RotL(cur);
      }
      
      return cur;
    }
    void erase(int val){
      top=del(top,val);
    }
};
