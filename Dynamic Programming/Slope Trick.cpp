

LL n,h,l[100005],r[100005],len[100005];

priority_queue<LL> lower;
priority_queue<LL,vector<LL>,greater<LL>> upper;

LL lazylow, lazyhigh;

int main(){
    cin >> n;
    for(int i = 1;i <= n;i++){
        cin >> l[i] >> r[i];
        len[i] = r[i]-l[i];
    }
    LL ans = 0;
    upper.push(l[1]); lower.push(l[1]);
    for(int i = 2;i <= n;i++){
        lazylow -= len[i];
        lazyhigh += len[i-1];
        if(l[i] <= lower.top()+lazylow){
            LL shifted = lower.top()+lazylow;
            ans += (shifted-l[i]);
            lower.push(l[i]-lazylow);
            lower.push(l[i]-lazylow);
            lower.pop();
            upper.push(shifted-lazyhigh);
        }else if(l[i] >= upper.top()+lazyhigh) {
            LL shifted = upper.top()+lazyhigh;
            ans += (l[i]-shifted);
            upper.push(l[i]-lazyhigh);
            upper.push(l[i]-lazyhigh);
            upper.pop();
            lower.push(shifted-lazylow);
        }else{
            lower.push(l[i]-lazylow);
            upper.push(l[i]-lazyhigh);
        }
    }
    cout << ans << endl;
    return 0;
}