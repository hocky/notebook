

LL n,k,curk,pos[1005];
LL sumpos[1005],sumisi[1005];
LL memo[1005][1005],isi[1005];

LL cost(LL a, LL b){
    LL ret = (sumisi[b]-sumisi[a-1])*pos[b];
    ret -= (sumpos[b]-sumpos[a-1]);
    return ret;
}

void calc(LL kiri, LL kanan, LL optl, LL optr){
    if(kiri > kanan) return;
    LL mid = (kiri+kanan)/2; LL optm;
    for(int i = optl;i <= min(mid,optr);i++){
        LL nx = memo[curk-1][i-1]+cost(i,mid);
        if(memo[curk][mid] > nx){
            memo[curk][mid] = nx;
            optm = i;
        }
    }
    calc(kiri,mid-1,optl,optm);
    calc(mid+1,kanan,optm,optr);
}

//dp(n,k) = minimum cost from 1 to n, using k thing
//It is know that the bigger the k is, the smaller the dp will be
//dp(n,k-1) <= dp(n,k) <= dp(n,k+1);
//dp(n,k) = allmin[i < n](dp(i,k-1)+c(i+1,n))
//opt(n,k) = the first i the dp(n,k) answer's found
//opt(n-1,k) <= opt(n,k) <= opt(n+1,k);

int main(){
    cin >> n >> k;
    for(int i = 1;i <= n;i++){
        cin >> pos[i] >> isi[i];
        sumpos[i] = sumpos[i-1]+(pos[i]*isi[i]);
        sumisi[i] = sumisi[i-1]+isi[i];
    }
    memset(memo,63,sizeof(memo));
    for(int i = 0;i <= k+1;i++) memo[i][0] = 0;
    for(int i = 1;i <= k;i++){
        curk = i;
        calc(1,n,1,n);
    }
    cout << memo[k][n] << endl;
    return 0;
}