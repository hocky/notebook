void make_edge(PLL u, PLL v){
    LL uidx = u.fi+(u.se*n);
    LL vidx = v.fi+(v.se*n);
    edge[uidx].pb(vidx);
    uidx = u.fi+((!u.se)*n);
    vidx = v.fi+((!v.se)*n);
    edge[vidx].pb(uidx);
}
    //Make implication graph based on boolean, PLL .se is boolean of negation
    // Do SCC on all vertex
    for(int i = 1;i <= n;i++){
        //sccidx is topological order already right, because we use tarjan
        //semakin kecil, dia semakin diujung, harus dikasi true
        if(sccidx[i] == sccidx[i+n]) cant();
        if(sccidx[i] > sccidx[i+n]) ans[i] = 1;
    }