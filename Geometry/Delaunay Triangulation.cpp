struct edge {
    point p[2];
    bool del;
    edge(point a, point b): p({a, b}), del(false) {}
    edge() {}
    bool operator ==(edge o) const {
        return (p[0] == o.p[0] && p[1] == o.p[1]);
    }
    bool s_equal(edge o) {
        return *this == o || edge(p[1], p[0]) == o;
    }
};

struct triangle {
    point p[3];
    edge e[3];
    bool del;
    triangle(point a, point b, point c): p({a, b, c}),e({edge(a, b), edge(b, c), edge(c, a)}),del(false) {}

    triangle() {}

    bool contains_point(point po) {
        for (int i = 0; i < 3; i++) {
            if (p[i] == po) return true;
        }
        return false;
    }

    bool cc_contains(point po) {
        double ab = norm_sq(vec(point(0, 0), p[0]));
        double cd = norm_sq(vec(point(0, 0), p[1]));
        double ef = norm_sq(vec(point(0, 0), p[2]));

        double circum_x = 
        (ab * (p[2].y - p[1].y) + cd * (p[0].y - p[2].y) + ef * (p[1].y - p[0].y)) /
        (p[0].x * (p[2].y - p[1].y) + p[1].x * (p[0].y - p[2].y) + p[2].x * (p[1].y - p[0].y));
        double circum_y =
        (ab * (p[2].x - p[1].x) + cd * (p[0].x - p[2].x) + ef * (p[1].x - p[0].x)) /
        (p[0].y * (p[2].x - p[1].x) + p[1].y * (p[0].x - p[2].x) + p[2].y * (p[1].x - p[0].x));

        point circum = point(circum_x / 2, circum_y / 2);
        double circum_radius = sqrt(norm_sq(vec(p[0], circum)));
        double distance_to_center = sqrt(norm_sq(vec(po, circum)));

        return distance_to_center <= circum_radius;
    }
};

// Implementation of the Bowyer–Watson algorithm
// Algorithm Complexity: O(N^2)

// There exist a DnC algorithm with better complexity at worse case O(N log N),
// but it involves a complex data structure and an code of overwhelming length of which,
// in terms of typing speed isn't suitable to the competitive programming scene.

vector<triangle> delaunay_triangulate(vector<point> &points) {
    vector<triangle> triangles;

    // Determine the super triangle
    double min_x, max_x;
    double min_y, max_y;

    min_x = max_x = points[0].x;
    min_y = max_y = points[0].y;

    for (point p : points) {
        min_x = min(min_x, p.x);
        max_x = max(max_x, p.x);
        min_y = min(min_y, p.y);
        max_y = max(max_y, p.y);
    }

    double dx = max_x - min_x;
    double dy = max_y - min_y;
    double dmax = max(dx, dy);
    double mid_x = (min_x + max_x) / 2;
    double mid_y = (min_y + max_y) / 2;

    point p1 = point(mid_x - 20*dmax, mid_y - dmax);
    point p2 = point(mid_x, mid_y + 20*dmax);
    point p3 = point(mid_x + 20*dmax, mid_y - dmax);

    triangles.push_back(triangle(p1, p2, p3));

    // Iteratively add points
    for (point p : points) {
        vector<edge> polygon(0);

        // Delete all triangles which contains point p
        for (triangle &t : triangles) {
            if (t.cc_contains(p)) {
                t.del = true;
                polygon.push_back(t.e[0]);
                polygon.push_back(t.e[1]);
                polygon.push_back(t.e[2]);
            }
        }

        triangles.erase(remove_if(triangles.begin(), triangles.end(), [](triangle &t) {
            return t.del;
        }), triangles.end());

        // Delete all double edges (edges which are actually erased)
        for (int i = 0; i < (int) polygon.size(); i++) {
            for (int j = i+1; j < (int) polygon.size(); j++) {
                edge &e1 = polygon[i];
                edge &e2 = polygon[j];
                if (e1.s_equal(e2)) {
                    e1.del = true;
                    e2.del = true;
                }
            }
        }

        polygon.erase(remove_if(polygon.begin(), polygon.end(), [](edge &e) {
            return e.del;
        }), polygon.end());

        // Add triangles from leftover edges connecting to the current point p
        for (edge e : polygon) {
            triangles.push_back(triangle(e.p[0], e.p[1], p));
        }
    }

    // Delete triangles associated with first super triangle
    triangles.erase(remove_if(triangles.begin(), triangles.end(), [p1, p2, p3](triangle &t) {
        return t.contains_point(p1) || t.contains_point(p2) || t.contains_point(p3);
    }), triangles.end());

    return triangles;
}
