const int MAXN = 1000005;
char s[MAXN];
int odd[MAXN], even[MAXN]; // odd centered at i, even with right center at i
int len;
void manacher() {
  // construct odd length
  for(int i = 0, l = 0, r = -1 ; i < len ; i++) {
    int k = (i > r ? 1 : min(odd[l + r - i],r - i));
    while(i + k < len && i - k >= 0 && s[i + k] == s[i - k]) k++;
    odd[i] = k--;
    if(i + k > r){
      l = i - k;
      r = i + k;
    }  
  }
  //construct even length
  for(int i = 0, l = 0, r = -1 ; i < len ; i++) {
    int k = (i > r ? 0 : min(even[l + r - i + 1],r - i + 1)) + 1;
    while(i + k - 1 < len && i - k >= 0 && s[i + k - 1] == s[i - k]) k++;
    even[i] = --k;
    if(i + k - 1 > r) {
      l = i - k;
      r = i + k - 1;
    }  
  }  
}
int main() {
  scanf("%s",s);
  len = strlen(s);
  manacher();
  for(int i = 0 ; i < len ; i++) {
    printf("i %d -> %d %d\n",i,odd[i],even[i]);
  }
  return 0;
}
