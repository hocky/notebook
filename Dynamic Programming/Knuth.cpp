int main(){
    for(int i = 1;i < n;i++){
        memo[i][i+1] = isi[i]+isi[i+1];
        opt[i][i+1] = i;
    }
    for(int i = 2;i <= n;i++){
        //Compute for i+1 segment
        for(int j = 1;j+i <= n;j++){
            LL cur = LINF;
            for(int k = opt[j][j+i-1];k <= opt[j+1][j+i];k++){
                LL now = memo[j][k]+memo[k+1][j+i];
                if(cur > now){
                    cur = now;
                    opt[j][j+i] = k;
                }
            }
            memo[j][j+i] = cur+(psum[j+i]-psum[j-1]);
        }
    }
}