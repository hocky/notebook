LL n,isi[45];
LL ans,maxheur;
LL jarak[45][45];
LL d[4][4] = {{-3,-1,4,-4},{1,3,4,-4},{1,-1,4,-4},{1,-1,4,-4}};

LL ida(LL pos, LL pre, LL step, LL curheur){
    if(curheur == 0){
        ans = step;
        return step;
    }
    if(step+curheur > maxheur) return step+curheur;
    LL nxheur = LINF;
    for(int i = 0;i < 4;i++){
        LL nx = pos+d[pos%4][i];
        if(nx == pre) continue;
        if(nx <= 0 || nx > n) continue;
        LL heuristic = curheur-jarak[nx][isi[nx]];
        swap(isi[pos],isi[nx]);
        heuristic += jarak[pos][isi[pos]];
        LL tmp = ida(nx,pos,step+1,heuristic);
        if(ans) return ans;
        swap(isi[pos],isi[nx]);
        nxheur = min(nxheur,tmp);
    }
    return nxheur;
}

int main(){
    for(int k = 1;k <= 40;k++){
        for(int i = 1;i <= 40;i++){
            for(int j = 1;j <= 40;j++){
                jarak[i][j] = min(jarak[i][j],jarak[i][k]+jarak[k][j]);
            }
        }
    }
    LL tc = 0;
    while(cin >> n){
        if(n == 0) break;
        ans = 0; LL curheur = 0; LL pos;
        cout << "Set " << ++tc << ":" << endl;
        for(int i = 1;i <= n;i++){
            cin >> isi[i];
            if(isi[i] != 1) curheur += jarak[i][isi[i]];
            else pos = i;
        }
        if(curheur == 0){
            cout << ans << endl;
            continue;
        }
        maxheur = curheur;
        while(!ans) maxheur = ida(pos,-1,0,curheur);
        cout << ans << endl;
    }
    return 0;
}