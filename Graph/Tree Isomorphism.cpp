
const LL base = 837482172LL;
const LL vbase = 409;
LL n,curroot,subsz[100005];
vector <LL> edge[100005];

LL getcent(LL pos, LL par,bool F){
    for(int i = 0;i < edge[pos].size();i++){
        LL nx = edge[pos][i];
        if(nx == par) continue;
        if(F){
            if(subsz[nx] > (subsz[curroot])/2) return getcent(nx,pos,F);
        }else{
            if(subsz[nx] >= (subsz[curroot]+1)/2) return getcent(nx,pos,F);
        }
    }
    return pos;
}

LL solve_hash(LL pos, LL par){
    vector <LL> child;
    for(int i = 0;i < edge[pos].size();i++){
        LL nx = edge[pos][i];
        if(nx == par) continue;
        child.pb(solve_hash(nx,pos));
    }
    sort(child.begin(),child.end());
    LL ret = vbase;
    for(int i = 0;i < child.size();i++){
        ret = (ret+(base^child[i]))%MOD;
    }
    return ret;
}