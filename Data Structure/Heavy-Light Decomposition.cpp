#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
int heavy[N]; // heaviest subtree in child
int size[N]; // size of subtree rooted in this vertice
int depth[N]; // depth of this vertice
int parent[N]; // parent in rooted tree
int headInChain[N]; // head in this vertice's path
int posInChain[N]; // position of this vertice in it's path, closer to root means lower position
int chainNumber[N]; // ID of path which contains this vertice
vector<int> adj[N]; // adjacency list
int n,q;

void dfs(int now) {
	heavy[now] = -1;
	size[now] = 1;
	for(int nex : adj[now]) {
		if(parent[now] == nex) continue;
		
		parent[nex] = now;
		depth[nex] = depth[now] + 1;
		dfs(nex);
 
		size[now] += size[nex];
		if(heavy[now] == -1 || size[heavy[now]] < size[nex]) {
			heavy[now] = nex;
		}
	}
}
void buildHLD() {
	int cntIndex = 0;
	
	dfs(1);
	for(int i = 1 ; i <= n ; i++) {
		if(i == 1 || heavy[parent[i]] != i) {
			int curPosition = 0;
			
			cntIndex++;
			for(int j = i ; j != -1 ; j = heavy[j]) {
				curPosition++;
				
				chainNumber[j] = cntIndex;
				headInChain[j] = i;
				posInChain[j] = curPosition;
			}
		}
		// can augment segtree / any DS heare
	}
}

// can query segtree / any DS in here
int getLCA(int u,int v) {
	while(chainNumber[u] != chainNumber[v]) {
		int headOfU = headInChain[u];
		int headOfV = headInChain[v];
		if(depth[headOfU] < depth[headOfV])
			swap(u,v);	
		u = parent[headInChain[u]];	
	}
	return depth[u] < depth[v] ? u : v;
}
