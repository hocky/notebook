void tarjanSCC(LL pos){
    path.pb(pos); vis[pos] = 1;
    low[pos] = num[pos] = ++dfscnt;
    for(int i = 0;i < edge[pos].size();i++){
        LL nx = edge[pos][i];
        if(!num[nx]) tarjanSCC(nx);
        if(vis[nx]) low[pos] = min(low[pos],low[nx]);
    }
    if(low[pos] == num[pos]){
        vector <LL> curans;
        while(1){
            LL now = path.back();
            path.popb();
            vis[now] = 0;
            if(now == pos) break;
        }
    }
}