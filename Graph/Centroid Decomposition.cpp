//centroid decomposition

void dfssz(LL pos, LL par){
    subsz[pos] = 1;
    for(int i = 0;i < edge[pos].size();i++){
        LL nx = edge[pos][i];
        if(nx == par || done[nx]) continue;
        dfssz(nx,pos);
        subsz[pos] += subsz[nx];
    }
    return;
}

LL getcent(LL pos, LL par){
    for(int i = 0;i < edge[pos].size();i++){
        LL nx = edge[pos][i];
        if(nx == par || done[nx]) continue;
        if(subsz[nx] > subsz[curroot]/2) return getcent(nx,pos);
    }
    return pos;
}

LL decompose(LL pos){
    curroot = pos;
    dfssz(pos,-1);
    pos = getcent(pos,-1);
    done[pos] = 1;
    for(int i = 0;i < edge[pos].size();i++){
        LL nx = edge[pos][i];
        if(done[nx]) continue;
        LL nxcent = decompose(nx);
        centedge[pos].pb(nxcent);
        centroidpar[nxcent] = pos;
    }
    return pos;
}


int main(){
    for(int i = 1;i <= n;i++) centroidpar[i] = -1;
    root = decompose(1);
    LL ans = 0;
    for(int i = 1;i <= n;i++){
        LL pos = i;
        LL ori = i;
        LL pre = -1;
        while(pos != -1){
            LL curdist = k-dist(ori,pos);
            if(curdist == 0) ans += centree[pos][curdist];
            if(curdist > 0){
                ans += mergecent[pos][curdist];
                if(pre != -1) ans -= centree[pre][curdist];
            }
            pre = pos;
            pos = centroidpar[pos];
        }
        pos = i;
        ori = i;
        centree[pos][0]++;
        while(centroidpar[pos] != -1){
            LL curdist = dist(ori,centroidpar[pos]);
            if(curdist <= k){
                centree[pos][curdist]++;
                mergecent[centroidpar[pos]][curdist]++;
            }
            pos = centroidpar[pos];
        }
    }
}