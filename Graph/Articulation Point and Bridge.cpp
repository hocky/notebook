void tarjan(LL pos,LL par){
  low[pos] = num[pos] = ++dfscnt;
  for(int i = 0;i < edge[pos].size();i++){
    LL nx = edge[pos][i];
    if(nx == par) continue;
    if(!num[nx]){
      if(pos == curroot) curchild++;
      tarjan(nx,pos);
      if(low[nx] >= num[pos]) isArticulationPoint[pos] = 1;
      // if(low[nx] > num[pos]) isBridge[pos][nx] = 1;
      low[pos] = min(low[pos],low[nx]);
    }else{
      //Use backedge
      low[pos] = min(low[pos],num[nx]);
    }
  }
}

int main(){
    for(int i = 1;i <= n;i++){
      if(num[i]) continue;
      curroot = i;
      curchild = 0;
      tarjan(i,-1);
      isArticulationPoint[i] = (curchild > 1);
    }
}
