void lcrs(LL pos,LL par){
    LL be = -1;
    for(int i = 0;i < edge[pos].size();i++){
        LL nx = edge[pos][i];
        if(par == nx) continue;
        lcrs(nx,pos);
        if(isi[pos].kiri == -1) isi[pos].kiri = nx;
        if(be != -1) isi[be].kanan = nx;
        be = nx;
    }
    return;
}
