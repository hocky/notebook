struct Point {
    int val[2];

    Point() {}

    long long get_dist(Point other) {
        long long ret = 0;
        for(int i = 0 ; i < 2 ; i++) {
            ret += 1ll * (val[i] - other.val[i]) * (val[i] - other.val[i]);
        }
        return ret;
    }
};

struct KDTree {
    vector<Point> points;
    int dim;
    KDTree(int _d = 1) {
        dim = _d;
        points.clear();
    }
    int get_size() {
        return points.size();
    }
    void add(vector<Point> v) {
        for(Point x : v) {
            points.push_back(x);
        }
    }
    void build() {
        build(0, 0, points.size());
    }
    void build(int depth, int l, int r) {
        if(l >= r) {
            return;
        }

        int modd = depth % dim;
        int mid = (l + r) / 2;
        nth_element(points.begin() + l, points.begin() + mid, points.begin() + r, [&](Point a, Point b) {
            return a.val[modd] < b.val[modd];
        });

        build(depth + 1, l, mid);
        build(depth + 1, mid+1, r);
    }

    void query(int depth, int l, int r, Point tgt, long long &best) {
        if(l >= r) {
            return;
        }

        int modd = depth % dim;
        int mid = (l + r) / 2;
        long long cur = tgt.get_dist(points[mid]);

        if(cur > 0)
            best = min(best, cur);

        int l1 = l, r1 = mid;
        int l2 = mid+1, r2 = r;

        // go to nearest subtree according to axis
        if(tgt.val[modd] > points[mid].val[modd]) {
            swap(l1, l2);
            swap(r1, r2);
        }

        query(depth+1, l1, r1, tgt, best);

        // traverse to other subtree if answer may be there
        int delta = tgt.val[modd] - points[mid].val[modd];
        long long delta2 = 1ll * delta * delta;

        if(delta2 < best) {
            query(depth+1, l2, r2, tgt, best);
        }
    }

    long long query(Point tgt) {
        long long ret = 4e18;

        query(0, 0, points.size(), tgt, ret);

        return ret;
    }
};