vector<int> treeAdj[2 * N];
int treeId[N];
int treeSize = 0;

int low[N], num[N];
vector<vector<int>> comps;
vector<int> stek;
bool isArt[N];

int n;

void tarjanBCC(int now) {
  static int cnt = 0;

  low[now] = num[now] = ++cnt;
  stek.push_back(now);

  for (int nex : adj[now]) {
    if (num[nex] != 0) {
      low[now] = min(low[now], num[nex]);
    } else {
      tarjanBCC(nex);

      if (low[nex] >= num[now]) {
        isArt[now] = (num[now] > 1 || num[nex] > 2);

        comps.push_back({now});
        while (comps.back().back() != nex) {
          comps.back().push_back(stek.back());
          stek.pop_back();
        }
      }

      low[now] = min(low[now], low[nex]);
    }
  }
}

void makeBCC() {
  for (int i = 1 ; i <= n ; i++) {
    if (num[i] == 0) {
      tarjanBCC(i);
    }
  }

  for (int i = 1 ; i <= n ; i++) {
    if (isArt[i]) {
      treeId[i] = ++treeSize;
    }
  }

  for (auto it : comps) {
    int node = ++treeSize;

    for (int x : it) {
      if (!isArt[x]) treeId[x] = node;
      else {
        treeAdj[node].push_back(treeId[x]);
        treeAdj[treeId[x]].push_back(node);
      }
    }
  }
}
