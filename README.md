# Quantasaurus Tiguerra Team Notebook

Repository for our team notebook in preparation for Regional Kuala Lumpur 2019. We used [codes2pdf](https://github.com/Erfaniaa/codes2pdf) to compile the codes and latex files into a pdf file.

Usual command (run on root of repo):

```bash
codes2pdf . --author="Quantasaurus Tiguerra - Universitas Indonesia" --initials="Quantasaurus Tiguerra - Universitas Indonesia"
```

Use at your own risk.