mt19937 rng(time(NULL));

struct node{
	LL prior,sz,val;
	LL l, r;
}treap[3000005];

struct dt{
	LL kiri,kanan;
};

LL tridx,root;

void updsz(LL now){
	if(now == 0) return;
	treap[now].sz = treap[treap[now].l].sz+treap[treap[now].r].sz+1;
}

dt split(LL now, LL tar){
	if(now == 0) return {0,0};
	dt ret;
	if(treap[now].val <= tar){
		dt tmp = split(treap[now].r,tar);
		treap[now].r = tmp.kiri;
		ret = {now,tmp.kanan};
	}else{
		dt tmp = split(treap[now].l,tar);
		treap[now].l = tmp.kanan;
		ret = {tmp.kiri,now};
	}
	updsz(now);
	return ret;
}

LL merge(LL l, LL r){
	LL ret;
	if(l == 0) ret = r;
	else if(r == 0) ret = l;
	else{
		if(treap[l].prior > treap[r].prior){
			treap[l].r = merge(treap[l].r,r);
			ret = l;
		}else{
			treap[r].l = merge(l,treap[r].l);
			ret = r;
		}
	}
	updsz(ret);
	return ret;
}

bool find(LL now, LL tar){
	while(now){
		if(treap[now].val == tar) return 1;
		if(treap[now].val < tar) now = treap[now].r;
		else now = treap[now].l;
	}
	return 0;
}

void insert(LL tar){
	if(find(root,tar)) return;
	LL cur = ++tridx;
	treap[cur].val = tar;
	treap[cur].prior = rng();
	dt nx = split(root,tar);
	LL tmp = merge(nx.kiri,cur);
	root = merge(tmp,nx.kanan);
}

void erase(LL tar){
	if(!find(root,tar)) return;
	dt nxa = split(root,tar);
	dt nxb = split(nxa.kiri,tar-1);
	root = merge(nxb.kiri,nxa.kanan);
}